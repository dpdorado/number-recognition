# Number recognition

Big Number recognition with python, using tensorflow and keras

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:3eb9fd82093cdf198c661714aff980ef?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:3eb9fd82093cdf198c661714aff980ef?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:3eb9fd82093cdf198c661714aff980ef?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/dpdorado/number-recognition.git
git branch -M main
git push -uf origin main
```

## Setting a virtual environment

> **1. Clone git lab repository**\
git clone https://gitlab.com/dpdorado/number-recognition.git

> **2. Create a new virtual environment**\
virtualenv env_name pyhton=pyhton3.8

> **3. Install packages in virtual environment**\
pip install -r requirements.txt

> **4. Add virtual environment to the pyhton kernel**\
python -m ipykernel install --user --name=env_name

> **5. Open jupyter notebook**\
jupyter notebook
